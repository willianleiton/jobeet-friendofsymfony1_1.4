<?php

// autoload_files.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    '2c102faa651ef8ea5874edb585946bce' => $vendorDir . '/swiftmailer/swiftmailer/lib/swift_required.php',
    '3c0e20b7ab865d179af142c39e3e1911' => $vendorDir . '/friendsofsymfony1/symfony1/autoload.php',
);
