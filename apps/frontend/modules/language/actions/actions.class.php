<?php

/**
 * language actions.
 *
 * @package    jobeet
 * @subpackage language
 * @author     Your name here
 * @version    SVN: $Id$
 */
class languageActions extends sfActions
{
  /**
   * Executes index action
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
    //dia 19
    public function executeChangeLanguage(sfWebRequest $request)
    {
        //dia 21
        $form = new sfFormLanguage($this->getUser(), array('languages' => array('en', 'fr')));
        $form->disableLocalCSRFProtection();

        $form->process($request);

        return $this->redirect('localized_homepage');
    }
}
