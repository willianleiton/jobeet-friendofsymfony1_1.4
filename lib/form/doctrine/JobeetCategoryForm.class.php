<?php

/**
 * JobeetCategory form.
 *
 * @package    jobeet
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
class JobeetCategoryForm extends BaseJobeetCategoryForm
{
    public function configure()
    {


        //dia 19
        unset(
            $this['jobeet_affiliates_list'],
            $this['created_at'], $this['updated_at']
        );

        $this->embedI18n(array('en', 'fr'));
        $this->widgetSchema->setLabel('en', 'English');
        $this->widgetSchema->setLabel('fr', 'French');

    }
}
