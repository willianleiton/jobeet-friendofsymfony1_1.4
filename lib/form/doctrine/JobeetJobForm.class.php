<?php

/**
 * JobeetJob form.
 *
 * @package    jobeet
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
class JobeetJobForm extends BaseJobeetJobForm
{
    //dia 10
    public function configure()
    {
        //dia 21
        $this->disableLocalCSRFProtection();

        //dia 12
        $this->removeFields();

        $this->validatorSchema['email'] = new sfValidatorAnd(array(
            $this->validatorSchema['email'],
            new sfValidatorEmail(),
        ));

        unset(
            $this['created_at'], $this['updated_at'],
            $this['expires_at'], $this['is_activated'],
            $this['token']
        );

        $this->validatorSchema['email'] = new sfValidatorAnd(array(
            $this->validatorSchema['email'],
            new sfValidatorEmail(),
        ));

        $this->widgetSchema['type'] = new sfWidgetFormChoice(array(
            'choices'  => Doctrine_Core::getTable('JobeetJob')->getTypes(),
            'expanded' => true,
        ));
        $this->validatorSchema['type'] = new sfValidatorChoice(array(
            'choices' => array_keys(Doctrine_Core::getTable('JobeetJob')->getTypes()),
        ));

        $this->widgetSchema['logo'] = new sfWidgetFormInputFile(array(
            'label' => 'Company logo',
        ));

        $this->widgetSchema->setLabels(array(
            'category_id'    => 'Category',
            'is_public'      => 'Public?',
            'how_to_apply'   => 'How to apply?',
        ));

        $this->validatorSchema['logo'] = new sfValidatorFile(array(
            'required'   => false,
            'path'       => sfConfig::get('sf_upload_dir').'/jobs',
            'mime_types' => 'web_images',
        ));

        $this->widgetSchema->setHelp('is_public', 'Whether the job can also be published on affiliate websites or not.');

        $this->widgetSchema->setNameFormat('job[%s]');
    }

    //dia 12
    protected function removeFields()
    {
        unset(
            $this['created_at'], $this['updated_at'],
            $this['expires_at'], $this['is_activated'],
            $this['token']
        );
    }
}
